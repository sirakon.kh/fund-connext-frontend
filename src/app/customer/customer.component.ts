import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.sass']
})
export class CustomerComponent implements OnInit {
  icon = 'pe-7s-cloud icon-gradient bg-tempting-azure';
  constructor() { }
  customerall = [
    {id: 1, customer: 'ONE ID', human_resource: true, status: 'enough', create_date: '2019-06-17'},
    {id: 1, customer: 'INET', human_resource: false, status: 'not enough', create_date: '2019-06-17'},
    {id: 1, customer: 'One DC', human_resource: true, status: 'nough', create_date: '2019-06-17'},
    {id: 1, customer: 'Talk to me', human_resource: true, status: 'enough', create_date: '2019-06-17'},
    {id: 1, customer: 'Openlandscape', human_resource: false, status: 'not enough', create_date: '2019-06-17'}
  ];


   taskdaily = [
    {subject: 'BOM', create_date: '2018-06-17', create_by: 'Mr. Santipap  Thongmai'},
    {subject: 'manage service', create_date: '2018-06-17', create_by: 'Mr. Thanaphon Atharattanasak'},
    {subject: 'centralized log', create_date: '2018-06-17', create_by: 'Mr. Chindanai Amonwatee'},
    {subject: 'VAC', create_date: '2018-06-17', create_by: 'Mr. Wasin  Poklin'},
    {subject: 'capa', create_date: '2018-06-17', create_by: 'Mr. Sirakon  Khumaidoung'},
    {subject: 'inet solution', create_date: '2018-06-17', create_by: 'Ms. Thananya Supriyasilp'},
    {subject: 'script', create_date: '2018-06-17', create_by: 'Ms. Chatsakul Sombutpiboon'},
    {subject: 'monitoring', create_date: '2018-06-17', create_by: 'Ms. Shanakan  Phetrattanarangsi'},
    {subject: 'government', create_date: '2018-06-17', create_by: 'Mr. Thapanut Lurejunda'},
    {subject: 'network', create_date: '2018-06-17', create_by: 'Ms. Chatsakul Sombutpiboon'}
  ];
  tables = false;
	company_name = '';

  outsourceall = [
    {id: '#1', name: 'Mr. Thanaphon Atharattanasak', company: 'ONE ID', status: 'onsite', create_date: '2017-06-30', due_date: '2019-06-30'},
   	{
      id: '#10', name: 'Ms. Shanakan  Phetrattanarangsi',
      company: 'ONE ID', status: 'onsite', create_date: '2018-06-30', due_date: '2019-06-30'
    }
  ];
outsourcetotal = {
    all: 50,
    doing: 40,
    free: 10,
    resolve: 20
  };

  topage_profile (company_name) {
  	this.tables = true;
  	this.company_name = company_name;
  }

  backpage () {
  	this.tables = false;
  	this.company_name = '';
  }
  ngOnInit() {
  }

}
