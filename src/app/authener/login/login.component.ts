import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_service/authentication.service';
import { RouterLink, Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';
  error_msg = '';
  loading_status = false;
  roleuser = '';
  constructor(private authenticationService: AuthenticationService,
    private router: Router,
  ) { }

  setRoleUser(role: string) {
    localStorage.setItem('role', role);
    this.roleuser = role;
  }
  setCustomerId(role: string) {
    localStorage.setItem('customer_id', role);
    this.roleuser = role;
  }
  setUsername(role: string) {
    localStorage.setItem('username', role);
    this.roleuser = role;
  }
  setProjectId(role: string) {
    localStorage.setItem('project_id', role);
    this.roleuser = role;
  }

  login(): void {
    if (this.username && this.password) {
      this.loading_status = true;
      this.authenticationService.getState(this.username).subscribe(data => {
        this.authenticationService.login(this.username, this.password, data['state']).subscribe(responseLogin => {
          this.authenticationService.setAuthorizationToken(responseLogin);
          this.setRoleUser(responseLogin['role']);
          this.setUsername(responseLogin['username']);
          // console.log(responseLogin)
          if (responseLogin['role'] === 'admin') {
            this.router.navigate(['admin/customers']);
          } else {
            this.router.navigate(['overview']);
            this.setCustomerId(responseLogin['customer_id']);
            this.setProjectId(responseLogin['projects'][0]['project_id']);
          }

        },
          error => {
            this.loading_status = false;
            this.error_msg = error.error.message;
          });
      },
        error_state => {
          this.loading_status = false;
          this.error_msg = 'There was a problem connecting to the query server. please try again later';
        }
      );
    } else {
      this.error_msg = 'Please, enter your username and password';
    }
  }
  ngOnInit() {
  }

}
