import { Injectable } from '@angular/core';
import { finalize, tap } from 'rxjs/operators';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders,
  HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import {AuthenticationService} from '../_service/authentication.service';
import { Observable } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

/** Pass untouched request through to the next request handler. */
@Injectable({providedIn: 'root'})
export class TokenInterceptor implements HttpInterceptor {
    // cachedRequests: Array<HttpRequest<any>> = [];
    // public collectRequest(request): void {
    //     this.cachedRequests.push(request);
    // }
    constructor(private auth: AuthenticationService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // console.log('sssssssssssssssss')
        if (request.url.endsWith('/login')  || request.url.indexOf('/state/') >= 0)  {
            return next.handle(request);
        } else {
            /** Attach token even if token is available */
            const token = localStorage.getItem('access_token');
            // console.log('fffff',token)
            if (token) {
                const headers = new HttpHeaders({
                    'Authorization': token,
                    'Content-Type': 'application/json'
                  });
                request = request.clone({
                    headers
                });
            }
        }
        /** Return interceptor request */
        return next.handle(request);
    }
    }
