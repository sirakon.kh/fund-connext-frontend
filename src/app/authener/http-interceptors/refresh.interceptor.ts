/** Injectable */
import { Injectable } from '@angular/core';
/** HTTP */
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse,
} from '@angular/common/http';
/** RxJS */
import { Observable, throwError } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import {TokenInterceptor} from './token-interceptor';
import {AuthenticationService} from '../_service/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class RefreshInterceptor implements HttpInterceptor {
    /** Constructor */
    constructor(
        private _authenInterceptor: TokenInterceptor,
        private _callbackService: AuthenticationService,
    ) { }
    /** Intercept http request w/ set authorized header */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Check url white list before
        /** Return interceptor request */
        if (request.url.endsWith('/login')  || request.url.indexOf('/state/') >= 0 || request.url.endsWith('refresh'))  {
            return next.handle(request);
        } else {
            return next.handle(request).pipe(
                map(res => res),
                catchError((err: HttpErrorResponse) => {
                    if (err instanceof HttpErrorResponse) {
                        if (err.status === 401) {
                            return this._callbackService.refreshNewToken().pipe(mergeMap(() => {
                                return this._authenInterceptor.intercept(request, next);
                            }));
                        }
                    }
                    /** Throw error */
                    return throwError(err);
                })
            );
    }
}

    /** Matches function */
}

