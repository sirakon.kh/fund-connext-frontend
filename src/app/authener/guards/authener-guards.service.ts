import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../_service/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenerGuardsService implements CanActivate {

  constructor( private _authenService: AuthenticationService,
    private route: Router, private _router: Router,
  ) {

  }
  role = '';
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    /** If no any token store try to redirect to login page */
    // if (!this._authenService.isLoggedIn()) {
    //   localStorage.clear();
    //   this.route.navigate(['login']);
    //   return false;
    // }
    // return true;

    this.role = localStorage.getItem('role');

    if (this.role === 'admin') {
      return true;
    } else {
      this._router.navigate(['/overview']);
    }
  }
}
