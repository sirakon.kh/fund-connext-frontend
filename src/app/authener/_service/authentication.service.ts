import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }
  apiUrl = environment.apiUrl;
  getState(username: string): Observable<{}> {
    return this.http.get(this.apiUrl + '/state/' + username);
  }

  login(username: string, password: string, state: string): Observable<{}> {
    this.removeAuthorizationToken();
    return this.http.post(this.apiUrl + '/login', {
      username : ('{username}'),
      password : ('{password}'),
      state : ('{state}')
    });
  }

  logout() {
    this.removeAuthorizationToken();
    location.href = '/login';
  }

  refreshNewToken() {
    {
      const url = this.apiUrl + '/refresh';
      const data = {
        access_token: localStorage.getItem('access_token'),
        refresh_token: localStorage.getItem('refresh_token'),
      };
      const refreshObservable = this.http.post(url, data);
      const refreshSubject = new ReplaySubject(1);
      refreshSubject.subscribe(r => {
        this.setAuthorizationToken(r);
      }, (err) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401 || err.status === 403) {
            location.href = '/login';
          }
        }
      }
      );
      refreshObservable.subscribe(refreshSubject);
      return refreshSubject;
    }
  }
  setAuthorizationToken(token: {}) {
    localStorage.setItem('access_token', token['access_token']);
    localStorage.setItem('refresh_token', token['refresh_token']);
    localStorage.setItem('company_id', token['company_id']);
    localStorage.setItem('worker_id', token['worker_id']);
    localStorage.setItem('expire_at', token['expire_at']);
  }
  removeAuthorizationToken() {
    localStorage.clear();
  }
  getAccessToken(): string {
    return localStorage.getItem('access_token');
  }
  getRefreshToken(): string {
    return localStorage.getItem('refresh_token');
  }

  isLoggedIn(): boolean {
    /** Check valid parameter */
    const access_token = localStorage.getItem('access_token');
    const refresh_token = localStorage.getItem('refresh_token');
    const customer_id = localStorage.getItem('customer_id');
    const expire_at = localStorage.getItem('expire_at');
    if (access_token && refresh_token && customer_id && expire_at) {
      return true;
    }
    return false;
  }
}
