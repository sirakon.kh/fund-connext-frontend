


import { Component, OnInit } from '@angular/core';
import {Color} from 'ng2-charts/ng2-charts';

@Component({
  selector: 'app-dashboard-overview',
  templateUrl: './dashboard-overview.component.html',
  styleUrls: ['./dashboard-overview.component.sass']
})
export class DashboardOverviewComponent implements OnInit {
  heading = 'Overview';
  // subheading = 'This is an example dashboard created using build-in elements and components.';
  icon = 'pe-7s-home icon-gradient bg-tempting-azure';
  // public datasets = [
  //   {
  //     label: 'My First dataset',
  //     data: [65, 59, 80, 81, 46, 55, 38, 59, 80],
  //     datalabels: {
  //       display: false,
  //     },
  //
  //   }
  // ];
  constructor() { }
  outsourcetotal = {
    all: 50,
    doing: 40,
    free: 10
  };
  outsourceall = [
    {id: '#1', name: 'Mr. Thanaphon Atharattanasak', company: 'INET', status: 'doing', create_date: '2017-06-30', due_date: '2019-06-30'},
    {id: '#2', name: 'Mr. Thatsanai Prabphai', company: 'INET', status: 'doing', create_date: '2018-06-30', due_date: '2019-06-30'},
    {id: '#3', name: 'Mr. Chindanai Amonwatee', company: 'INET', status: 'available', create_date: '2018-07-30', due_date: '2019-06-30'},
    {id: '#4', name: 'Mr. Wasin  Poklin', company: 'INET', status: 'doing', create_date: '2018-06-30', due_date: '2019-06-30'},
    {id: '#5', name: 'Mr. Sirakon  Khumaidoung', company: 'INET', status: 'doing', create_date: '2018-06-30', due_date: '2019-06-30'},
    {id: '#6', name: 'Mr. Santipap  Thongmai', company: 'INET', status: 'available', create_date: '2018-06-30', due_date: '2019-06-30'},
    {id: '#7', name: 'Ms. Thananya Supriyasilp', company: 'INET', status: 'doing', create_date: '2018-06-30', due_date: '2019-06-30'},
    {id: '#8', name: 'Ms. Chatsakul Sombutpiboon', company: 'INET', status: 'available', create_date: '2018-06-30', due_date: '2019-06-30'},
    {id: '#9', name: 'Mr. Thapanut Lurejunda', company: 'INET', status: 'doing', create_date: '2018-06-30', due_date: '2019-06-30'},
    {
      id: '#10', name: 'Ms. Shanakan  Phetrattanarangsi',
      company: 'INET', status: 'doing', create_date: '2018-06-30', due_date: '2019-06-30'
    }
  ];
  taskdaily = [
    {subject: 'BOM', create_date: '2018-06-17', create_by: 'Mr. Santipap  Thongmai'},
    {subject: 'manage service', create_date: '2018-06-17', create_by: 'Mr. Thanaphon Atharattanasak'},
    {subject: 'centralized log', create_date: '2018-06-17', create_by: 'Mr. Chindanai Amonwatee'},
    {subject: 'VAC', create_date: '2018-06-17', create_by: 'Mr. Wasin  Poklin'},
    {subject: 'capa', create_date: '2018-06-17', create_by: 'Mr. Sirakon  Khumaidoung'},
    {subject: 'inet solution', create_date: '2018-06-17', create_by: 'Ms. Thananya Supriyasilp'},
    {subject: 'script', create_date: '2018-06-17', create_by: 'Ms. Chatsakul Sombutpiboon'},
    {subject: 'monitoring', create_date: '2018-06-17', create_by: 'Ms. Shanakan  Phetrattanarangsi'},
    {subject: 'government', create_date: '2018-06-17', create_by: 'Mr. Thapanut Lurejunda'},
    {subject: 'network', create_date: '2018-06-17', create_by: 'Ms. Chatsakul Sombutpiboon'}
  ];
  public lineChartColors: Color[] = [
    { // dark grey
      backgroundColor: 'rgba(247, 185, 36, 0.2)',
      borderColor: '#f7b924',
      borderCapStyle: 'round',
      borderDash: [],
      borderWidth: 4,
      borderDashOffset: 0.0,
      borderJoinStyle: 'round',
      pointBorderColor: '#f7b924',
      pointBackgroundColor: '#fff',
      pointHoverBorderWidth: 4,
      pointRadius: 6,
      pointBorderWidth: 5,
      pointHoverRadius: 8,
      pointHitRadius: 10,
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#f7b924',
    },
  ];
  public datasets = [
    {
      label: 'My First dataset',
      data: [65, 59, 80, 81, 46, 55, 38, 59, 80],
      datalabels: {
        display: false,
      },

    }
  ];
  public labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August'];
  public options = {
    layout: {
      padding: {
        left: 0,
        right: 8,
        top: 0,
        bottom: 0
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          display: false,
          beginAtZero: true
        },
        gridLines: {
          display: false
        }
      }],
      xAxes: [{
        ticks: {
          display: false
        },
        gridLines: {
          display: false
        }
      }]
    },
    legend: {
      display: false
    },
    responsive: true,
    maintainAspectRatio: false
  };
  ngOnInit() {
  }

}