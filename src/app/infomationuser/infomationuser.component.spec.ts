import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfomationuserComponent } from './infomationuser.component';

describe('InfomationuserComponent', () => {
  let component: InfomationuserComponent;
  let fixture: ComponentFixture<InfomationuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfomationuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfomationuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
