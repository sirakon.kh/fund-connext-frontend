import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-infomationuser',
  templateUrl: './infomationuser.component.html',
  styleUrls: ['./infomationuser.component.sass']
})
export class InfomationuserComponent implements OnInit {

  constructor(private http: HttpClient, private modalService: NgbModal) { }
  optionsmarry;
  closeResult: string;
  page1 = true;
  page2 = false;
  page3 = false;
  page4 = false;
  idType;
  idNumber;
  passportCountry = '';
  fullnameTH;
  fullnameEN;
  gender;
  birthday;
  email;
  telephone;
  national;
  maritalStatus;
  married;
  wfIdType;
  wfIdcard;
  optionidpassport;
  wfFullName;
  wfBirthday;
  wfCountry;
  wfTel;
  statusData = true;
  haveChildren;
  lengthson;
  optionidpassportChildren;
  adNo;
  adMo;
  adBuild;
  adFloor;
  adSoi;
  adRoad;
  adSubDistrict;
  adDistrict;
  adProvince;
  adPostalCode;
  adCountry;

  wkNo;
  wkMo;
  wkBuild;
  wkFloor;
  wkSoi;
  wkRoad;
  wkSubDistrict;
  wkDistrict;
  wkProvince;
  wkPostalCode;
  wkCountry;

  nwNo;
  nwMo;
  nwBuild;
  nwFloor;
  nwSoi;
  nwRoad;
  nwSubDistrict;
  nwDistrict;
  nwProvince;
  nwPostalCode;
  nwCountry;

  optionaddressnow;
  occupation;
  businessType;
  confirmTaxDeduction;
  incomeSourceCountry;
  incomeSource;
  monthlyIncomeLevel;
  committedMoneyLaundering;
  politicalRelatedPerson;
  rejectFinancialTransaction;
  mailAddress;
  mailAddressDisplay;
  // investmentObjective;

  mailNo;
  mailMo;
  mailBuild;
  mailFloor;
  mailSoi;
  mailRoad;
  mailSubDistrict;
  mailDistrict;
  mailProvince;
  mailPostalCode;
  mailCountry;

  suitabilityAnswerList;
  bankAccountNo;
  bankCode;
  investExp = '';

  incomeSourceSend = '';
  incomeSourceOther = '';
  wftitleOther = '';

  approveChildren = [];

  id_number = '0';
  jobsTable;
  dataInformation;
  typesBusinesss;
  whereincome;
  numbersons;
  checkin;
  job = ['เกษตรกร', 'พระ', 'เจ้าของกิจการ', 'แพทย์', 'กิจการครอบครัว', 'ข้าราชการ', 'แม่บ้าน/พ่อบ้าน', 'นักลงทุน', 'อื่นๆ'];
  typeBusiness = ['ค้าของเก่า', 'อาวุธยุทธภัณฑ์', 'คาสิโน/พนัน', 'สหกรณ์ / มูลนิธิ /สมาคม',
    'โรงแรม', 'สถาบันการเงิน', 'ประกันภัย / ประกันชีวิต', 'ธุรกิจนำเที่ยว', 'นายหน้าจัดหางาน', 'ค้าอัญมณี',
    'มหาวิทยาลัย / โรงเรียน / สถานศึกษา', 'อื่นๆ'];

  goPage2() {
    this.page1 = false;
    this.page2 = true;
    this.page3 = false;
    this.page4 = false;
  }
  goPage3() {
    this.page1 = false;
    this.page2 = false;
    this.page3 = true;
    this.page4 = false;
  }
  goPage4() {
    this.page1 = false;
    this.page2 = false;
    this.page3 = false;
    this.page4 = true;
  }

  backtopage3() {
    this.page1 = false;
    this.page2 = false;
    this.page3 = true;
    this.page4 = false;
  }
  backtopage2() {
    this.page1 = false;
    this.page2 = true;
    this.page3 = false;
    this.page4 = false;
  }
  backtopage1() {
    this.page1 = true;
    this.page2 = false;
    this.page3 = false;
    this.page4 = false;
  }




  inputChanged(event) {
    console.log(event);
  }


  checkbox() {
    console.log(this.whereincome);
  }

  getinformation() {
    // this.http.get('http://localhost:8000/api/v1/application/700').subscribe(data => {
    // this.http.get('http://localhost:8000/api/v1/application/754').subscribe(data => {
    // this.http.get('http://localhost:8000/api/v1/application/686').subscribe(data => {
    // this.http.get('http://localhost:8000/api/v1/application/814').subscribe(data => {
    this.http.get('http://localhost:8000/api/v1/application/796').subscribe(data => {
      // console.log(data);
      if (data) {
        this.statusData = false;
      }
      this.approveChildren = [];

      this.dataInformation = data;

      for (const chil of this.dataInformation.data.childrenInfo.children) {
        // console.log(chil);
        const CH = {
          identificationCardType: chil.identificationCardType.key,
          passportCountry: chil.passportCountry.code,
          cardNumber: chil.cardNumber,
          title: chil.title.key,
          thFirstName: chil.thFirstName,
          thLastName: chil.thLastName,
          birthDate: chil.birthDate.epoc,
          idCardExpiryDate: chil.birthDate.epoc
        };

        this.approveChildren.push(CH);

      }

      // console.log(this.approveChildren);

      this.idType = this.dataInformation.data.identificationCardType;
      this.passportCountry = this.dataInformation.data.cardNumber;
      this.idNumber = this.dataInformation.user.cid;
      this.fullnameTH = this.dataInformation.data.title.displayName + ' ' + this.dataInformation.user.firstName + '   ' + this.dataInformation.user.lastName;
      this.fullnameEN = this.dataInformation.data.title.key + ' ' + this.dataInformation.data.enFirstName + '   ' + this.dataInformation.data.enLastName;
      this.gender = this.dataInformation.data.gender.displayName;
      this.birthday = this.dataInformation.data.birthDate.formatted;
      this.email = this.dataInformation.data.email;
      this.telephone = this.dataInformation.user.telNo;
      this.national = this.dataInformation.data.nationality;
      this.maritalStatus = this.dataInformation.data.familyInfo.maritalStatus.displayName;

      if (this.dataInformation.data.familyInfo.maritalStatus.key === 'Married') {
        this.married = 'marry';
        if (this.dataInformation.data.familyInfo.identificationCardType.key === 'CITIZEN_CARD') {
          this.optionidpassport = 'nopass';
        } else {
          this.optionidpassport = 'pass';
          this.wfCountry = this.dataInformation.data.familyInfo.passportCountry.name;
        }
        this.wfFullName = this.dataInformation.data.familyInfo.title.displayName + ' ' + this.dataInformation.data.familyInfo.thFirstName + '   ' + this.dataInformation.data.familyInfo.thLastName;
        this.wfIdcard = this.dataInformation.data.familyInfo.cardNumber;
        // this.wfBirthday = this.dataInformation.data.familyInfo.
        // this.wfTel = this.dataInformation.data.familyInfo.
      } else {
        this.married = 'nomarry';
      }


      if (this.dataInformation.data.childrenInfo.children.length !== 0) {
        console.log('data');
        this.haveChildren = 'have';
        this.numbersons = this.dataInformation.data.childrenInfo.children;
        this.lengthson = this.dataInformation.data.childrenInfo.childrenAmount.key;
        // this.optionidpassportChildren = identificationCardType
        // console.log(data);
      } else {
        this.haveChildren = 'nohave';
      }

      for (const i of this.dataInformation.data.declarationInfo) {
        // console.log(i);
        // if (i.questionId === 'investmentObjective'){
        // }
        if (i.questionId === 'rejectFinancialTransaction') {
          this.rejectFinancialTransaction = i.answer[0];
        }
        if (i.questionId === 'committedMoneyLaundering') {
          this.committedMoneyLaundering = i.answer[0];
        }
        if (i.questionId === 'politicalRelatedPerson') {
          this.politicalRelatedPerson = i.answer[0];
        }

      }

      this.adNo = this.dataInformation.data.residence.no;
      this.adMo = this.dataInformation.data.residence.moo;
      this.adBuild = this.dataInformation.data.residence.building;
      this.adFloor = this.dataInformation.data.residence.floor;
      this.adSoi = this.dataInformation.data.residence.soi;
      this.adRoad = this.dataInformation.data.residence.road;
      this.adSubDistrict = this.dataInformation.data.residence.subDistrict;
      this.adDistrict = this.dataInformation.data.residence.district;
      this.adProvince = this.dataInformation.data.residence.province;
      this.adPostalCode = this.dataInformation.data.residence.postalCode;
      this.adCountry = this.dataInformation.data.residence.country;

      this.wkNo = this.dataInformation.data.work.no;
      this.wkMo = this.dataInformation.data.work.moo;
      this.wkBuild = this.dataInformation.data.work.building;
      this.wkFloor = this.dataInformation.data.work.floor;
      this.wkSoi = this.dataInformation.data.work.soi;
      this.wkRoad = this.dataInformation.data.work.road;
      this.wkSubDistrict = this.dataInformation.data.work.subDistrict;
      this.wkDistrict = this.dataInformation.data.work.district;
      this.wkProvince = this.dataInformation.data.work.province;
      this.wkPostalCode = this.dataInformation.data.work.postalCode;
      this.wkCountry = this.dataInformation.data.work.country;


      this.nwNo = this.dataInformation.data.contact.no;
      this.nwMo = this.dataInformation.data.contact.moo;
      this.nwBuild = this.dataInformation.data.contact.building;
      this.nwFloor = this.dataInformation.data.contact.floor;
      this.nwSoi = this.dataInformation.data.contact.soi;
      this.nwRoad = this.dataInformation.data.contact.road;
      this.nwSubDistrict = this.dataInformation.data.contact.subDistrict;
      this.nwDistrict = this.dataInformation.data.contact.district;
      this.nwProvince = this.dataInformation.data.contact.province;
      this.nwPostalCode = this.dataInformation.data.contact.postalCode;
      this.nwCountry = this.dataInformation.data.contact.country;

      this.optionaddressnow = this.dataInformation.data.contactAddressSameAsFlag.key;
      this.occupation = this.dataInformation.data.financialInfo[0].answer[0].label;
      this.businessType = this.dataInformation.data.financialInfo[1].answer[0].label;

      this.confirmTaxDeduction = this.dataInformation.data.otherAccountInfo.confirmTaxDeduction.displayName;
      this.incomeSource = this.dataInformation.data.financialInfo[4].answer;
      this.incomeSourceCountry = this.dataInformation.data.financialInfo[5].answer[0].label;
      this.monthlyIncomeLevel = this.dataInformation.data.financialInfo[3].answer[0].label;

      // this.committedMoneyLaundering = this.dataInformation.data.declarationInfo[2].answer[0].label;
      // this.politicalRelatedPerson = this.dataInformation.data.declarationInfo[3].answer[0].label;
      // this.rejectFinancialTransaction = this.dataInformation.data.declarationInfo[1].answer[0].label;

      this.mailNo = this.dataInformation.data.mailing.no;
      this.mailMo = this.dataInformation.data.mailing.moo;
      this.mailBuild = this.dataInformation.data.mailing.building;
      this.mailFloor = this.dataInformation.data.mailing.floor;
      this.mailSoi = this.dataInformation.data.mailing.soi;
      this.mailRoad = this.dataInformation.data.mailing.road;
      this.mailSubDistrict = this.dataInformation.data.mailing.subDistrict;
      this.mailDistrict = this.dataInformation.data.mailing.district;
      this.mailProvince = this.dataInformation.data.mailing.province;
      this.mailPostalCode = this.dataInformation.data.mailing.postalCode;
      this.mailCountry = this.dataInformation.data.mailing.country;

      this.mailAddress = this.dataInformation.data.mailingAddressSameAsFlag.key;
      this.mailAddressDisplay = this.dataInformation.data.mailingAddressSameAsFlag.displayName;

      this.suitabilityAnswerList = this.dataInformation.data.suitabilityAnswerList;

      // for (const exp of this.dataInformation.data.suitabilityAnswerList[4].answer) {
      //   this.investExp += '-   ' + exp.label;
      //   console.log(this.investExp);
      // }
      this.dataInformation.data.suitabilityAnswerList[4].answer.forEach((item, index) => {

        if (index === 0) {
          this.investExp = item.label;
        } else {
          this.investExp += '  ,  ' + item.label;
        }

      });

      // this.dataInformation.data.suitabilityAnswerList[4] =
      this.suitabilityAnswerList[3].answer[0].label = this.investExp;
      this.bankAccountNo = this.dataInformation.data.otherAccountInfo.subscriptionBankAccounts.bankAccountNo;
      this.bankCode = this.dataInformation.data.otherAccountInfo.subscriptionBankAccounts.bankCode;




      this.dataInformation.data.financialInfo[4].answer.forEach((item, index) => {
        if (index === 0) {
          this.incomeSourceSend = item.id;
        } else {
          this.incomeSourceSend += ',' + item.id;
        }

        if (item.id === 'OTHER') {
          this.incomeSourceOther = item.label;
        }

      });




      console.log(this.incomeSourceSend);
      console.log(this.incomeSourceOther);

    });
  }

  approve(content) {
    for (const iterator of this.dataInformation.data.familyInfo) {
      if (iterator.key === 'OTHER') {
        this.wftitleOther = iterator.displayName;
      }
    }


    // this.dataInformation.data.familyInfo.forEach((item, index) => {

    //   if (item.key === 'OTHER') {
    //     this.wftitleOther = item.displayName;
    //   }

    // });

    // 'REJECT';

    const json_txt = {
      status: 'APPROVED',
      detail: '',
      username: 'kk',
      dict_data: {
        identificationCardType: this.idType,
        passportCountry: '',
        cardNumber: this.idNumber,
        cardExpiryDate: this.dataInformation.data.cardExpiryDate.epoc,
        accompanyingDocument: this.idType,
        gender: this.gender,
        title: this.dataInformation.data.title.displayName,
        titleOther: this.dataInformation.data.title.displayName,
        enFirstName: this.dataInformation.data.enFirstName,
        enLastName: this.dataInformation.data.enLastName,
        thFirstName: this.dataInformation.data.thFirstName,
        thLastName: this.dataInformation.data.thLastName,
        birthDate: this.dataInformation.data.birthDate.epoc,
        nationality: this.dataInformation.data.nationality,
        mobileNumber: this.dataInformation.data.mobileNumber,
        email: this.dataInformation.data.email,
        maritalStatus: this.dataInformation.data.familyInfo.maritalStatus.key,
        spouse: {
          identificationCardType: this.dataInformation.data.familyInfo.identificationCardType.key,
          passportCountry: 'this.dataInformation.data.familyInfo',
          cardNumber: this.dataInformation.data.familyInfo.cardNumber,
          title: this.dataInformation.data.familyInfo.title.key,
          wftitleOther: this.wftitleOther,
          thFirstName: this.dataInformation.data.familyInfo.thFirstName,
          thLastName: this.dataInformation.data.familyInfo.thLastName,
          phoneNumber: this.dataInformation.data.familyInfo.phoneNumber,
          idCardExpiryDate: this.dataInformation.data.familyInfo.idCardExpiryDate.epoc

        },
        occupationId: this.dataInformation.data.financialInfo[0].answer[0].id,
        occupationOther: this.dataInformation.data.financialInfo[0].answer[0].label,
        businessTypeId: this.dataInformation.data.financialInfo[1].answer[0].id,
        businessTypeOther: this.dataInformation.data.financialInfo[1].answer[0].label,
        monthlyIncomeLevel: this.dataInformation.data.financialInfo[3].answer[0].id,
        incomeSource: this.incomeSourceSend,
        incomeSourceOther: this.incomeSourceOther,
        residence: {
          no: this.dataInformation.data.residence.no,
          road: this.dataInformation.data.residence.road,
          subDistrict: this.dataInformation.data.residence.subDistrict,
          district: this.dataInformation.data.residence.district,
          province: this.dataInformation.data.residence.province,
          postalCode: this.dataInformation.data.residence.postalCode,
          country: this.dataInformation.data.residence.country
        },
        currentAddressSameAsFlag: this.dataInformation.data.contactAddressSameAsFlag.key,
        current: '',
        companyName: this.dataInformation.data.financialInfo[2].answer[0].label,
        workAddressSameAsFlag: '',
        work: {
          no: this.dataInformation.data.work.no,
          road: this.dataInformation.data.work.road,
          subDistrict: this.dataInformation.data.work.subDistrict,
          district: this.dataInformation.data.work.district,
          province: this.dataInformation.data.work.province,
          postalCode: this.dataInformation.data.work.postalCode,
          country: this.dataInformation.data.work.country
        },
        committedMoneyLaundering: this.committedMoneyLaundering,
        politicalRelatedPerson: this.politicalRelatedPerson,
        rejectFinancialTransaction: this.rejectFinancialTransaction,
        canAcceptFxRisk: '',
        canAcceptDerivativeInvestment: '',
        suitabilityRiskLevel: this.dataInformation.data.suitabilityRiskLevel,
        suitabilityEvaluationDate: '',
        fatca: this.dataInformation.data.fatcaAnswers.fatca,
        fatcaDeclarationDate: this.dataInformation.data.fatcaAnswers.fatcaDeclarationDate,
        cddScore: '',
        cddDate: '',
        referralPerson: '',
        applicationDate: '',
        incomeSourceCountry: this.dataInformation.data.financialInfo[5].answer[0].id,
        acceptBy: '',
        children: this.approveChildren,
        openFundConnextFormFlag: '',
        approved: true,
        vulnerableFlag: '',
        vulnerableDetail: '',
        ndidFlag: '',
        ndidRequestId: '',
        suitability: ''


      }
    };

    console.log(json_txt);
    // this.http.post('http://localhost:8000/api/v1/application/status/' + '796', json_txt).subscribe(data => {
    //   console.log(data);
    // });
    // this.clearModal(content);
  }

  reject(content) {
    for (const iterator of this.dataInformation.data.familyInfo) {
      if (iterator.key === 'OTHER') {
        this.wftitleOther = iterator.displayName;
      }
    }


    // this.dataInformation.data.familyInfo.forEach((item, index) => {

    //   if (item.key === 'OTHER') {
    //     this.wftitleOther = item.displayName;
    //   }

    // });

    // 'REJECT';

    const json_txt = {
      status: 'REJECTED',
      detail: '',
      username: 'kk',
      dict_data: {
        identificationCardType: this.idType,
        passportCountry: '',
        cardNumber: this.idNumber,
        cardExpiryDate: this.dataInformation.data.cardExpiryDate.epoc,
        accompanyingDocument: this.idType,
        gender: this.gender,
        title: this.dataInformation.data.title.displayName,
        titleOther: this.dataInformation.data.title.displayName,
        enFirstName: this.dataInformation.data.enFirstName,
        enLastName: this.dataInformation.data.enLastName,
        thFirstName: this.dataInformation.data.thFirstName,
        thLastName: this.dataInformation.data.thLastName,
        birthDate: this.dataInformation.data.birthDate.epoc,
        nationality: this.dataInformation.data.nationality,
        mobileNumber: this.dataInformation.data.mobileNumber,
        email: this.dataInformation.data.email,
        maritalStatus: this.dataInformation.data.familyInfo.maritalStatus.key,
        spouse: {
          identificationCardType: this.dataInformation.data.familyInfo.identificationCardType.key,
          passportCountry: 'this.dataInformation.data.familyInfo',
          cardNumber: this.dataInformation.data.familyInfo.cardNumber,
          title: this.dataInformation.data.familyInfo.title.key,
          wftitleOther: this.wftitleOther,
          thFirstName: this.dataInformation.data.familyInfo.thFirstName,
          thLastName: this.dataInformation.data.familyInfo.thLastName,
          phoneNumber: this.dataInformation.data.familyInfo.phoneNumber,
          idCardExpiryDate: this.dataInformation.data.familyInfo.idCardExpiryDate.epoc

        },
        occupationId: this.dataInformation.data.financialInfo[0].answer[0].id,
        occupationOther: this.dataInformation.data.financialInfo[0].answer[0].label,
        businessTypeId: this.dataInformation.data.financialInfo[1].answer[0].id,
        businessTypeOther: this.dataInformation.data.financialInfo[1].answer[0].label,
        monthlyIncomeLevel: this.dataInformation.data.financialInfo[3].answer[0].id,
        incomeSource: this.incomeSourceSend,
        incomeSourceOther: this.incomeSourceOther,
        residence: {
          no: this.dataInformation.data.residence.no,
          road: this.dataInformation.data.residence.road,
          subDistrict: this.dataInformation.data.residence.subDistrict,
          district: this.dataInformation.data.residence.district,
          province: this.dataInformation.data.residence.province,
          postalCode: this.dataInformation.data.residence.postalCode,
          country: this.dataInformation.data.residence.country
        },
        currentAddressSameAsFlag: this.dataInformation.data.contactAddressSameAsFlag.key,
        current: '',
        companyName: this.dataInformation.data.financialInfo[2].answer[0].label,
        workAddressSameAsFlag: '',
        work: {
          no: this.dataInformation.data.work.no,
          road: this.dataInformation.data.work.road,
          subDistrict: this.dataInformation.data.work.subDistrict,
          district: this.dataInformation.data.work.district,
          province: this.dataInformation.data.work.province,
          postalCode: this.dataInformation.data.work.postalCode,
          country: this.dataInformation.data.work.country
        },
        committedMoneyLaundering: this.committedMoneyLaundering,
        politicalRelatedPerson: this.politicalRelatedPerson,
        rejectFinancialTransaction: this.rejectFinancialTransaction,
        canAcceptFxRisk: '',
        canAcceptDerivativeInvestment: '',
        suitabilityRiskLevel: this.dataInformation.data.suitabilityRiskLevel,
        suitabilityEvaluationDate: '',
        fatca: this.dataInformation.data.fatcaAnswers.fatca,
        fatcaDeclarationDate: this.dataInformation.data.fatcaAnswers.fatcaDeclarationDate,
        cddScore: '',
        cddDate: '',
        referralPerson: '',
        applicationDate: '',
        incomeSourceCountry: this.dataInformation.data.financialInfo[5].answer[0].id,
        acceptBy: '',
        children: this.approveChildren,
        openFundConnextFormFlag: '',
        approved: true,
        vulnerableFlag: '',
        vulnerableDetail: '',
        ndidFlag: '',
        ndidRequestId: '',
        suitability: ''


      }
    };

    console.log;
    this.http.post('http://localhost:8000/api/v1/application/status/' + '796', json_txt).subscribe(data => {
      console.log(data);
    });
    this.clearModal(content);
  }

  openApprove(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }


  openReject(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  clearModal(content) {
    content('Close click');
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  ngOnInit() {
    this.getinformation();
  }

}
