import { Component, OnInit } from '@angular/core';
import {Color} from 'ng2-charts/ng2-charts';

@Component({
  selector: 'app-companyprofile',
  templateUrl: './companyprofile.component.html',
  styleUrls: ['./companyprofile.component.sass']
})
export class CompanyprofileComponent implements OnInit {
	company = 'One ID';
	icon = 'pe-7s-cloud icon-gradient bg-tempting-azure';
	  constructor() { }
	  customerall = [
	    {id: 1, customer: 'ONE ID', human_resource: true, status: 'enough', create_date: '2019-06-17'},
	    {id: 1, customer: 'INET', human_resource: false, status: 'not enough', create_date: '2019-06-17'},
	    {id: 1, customer: 'One DC', human_resource: true, status: 'nough', create_date: '2019-06-17'},
	    {id: 1, customer: 'Talk to me', human_resource: true, status: 'enough', create_date: '2019-06-17'},
	    {id: 1, customer: 'Openlandscape', human_resource: false, status: 'not enough', create_date: '2019-06-17'}
	  ];

	  tables = true;


	  outsourceall = [
	    {id: '#1', name: 'Mr. Thanaphon Atharattanasak', company: 'ONE ID', status: 'onsite', create_date: '2017-06-30', due_date: '2019-06-30'},
	   	{
	      id: '#10', name: 'Ms. Shanakan  Phetrattanarangsi',
	      company: 'ONE ID', status: 'onsite', create_date: '2018-06-30', due_date: '2019-06-30'
	    }
	  ];
	outsourcetotal = {
	    all: 50,
	    doing: 40,
	    free: 10,
	    resolve: 20
	  };	
  ngOnInit() {
  }
}
