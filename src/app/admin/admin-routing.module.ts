import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverviewComponent } from './overview/overview.component';
import { UserComponent } from './user/user.component';
import { UserprofileComponent } from './userprofile/userprofile.component';

const routes: Routes = [
  // {path: 'admin',
  // canActivate: [AuthenerGuardsService],
  // children: [
    {
      path: '',
      component: OverviewComponent,
    },
    {
      path: 'user',
      component: UserComponent,
    },
    {
      path: 'userprofile',
      component: UserprofileComponent,
    }
  // ]
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
