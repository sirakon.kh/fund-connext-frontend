import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { OverviewComponent } from './overview/overview.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { UserComponent } from './user/user.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
// import { AdminComponent } from './admin.component';

@NgModule({
  declarations: [
    OverviewComponent,
    UserComponent,
    UserprofileComponent,
    // AdminComponent
  ],
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
