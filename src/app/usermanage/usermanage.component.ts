import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-usermanage',
  templateUrl: './usermanage.component.html',
  styleUrls: ['./usermanage.component.sass']
})
export class UsermanageComponent implements OnInit {
  icon = 'pe-7s-users icon-gradient bg-tempting-azure';
  closeResult: string;
  constructor(private modalService: NgbModal) { }
  check_error = false;
  result_create = '';
  userall = [
    {
      id: '#1', name: 'Mr. Thanaphon Atharattanasak', username: 'Thanaphon',
      status: 'in progress', create_date: '2017-06-30', due_date: '2019-06-30'
    },
    {
      id: '#2', name: 'Mr. Thatsanai Prabphai', username: 'Thatsanai',
      status: 'in progress', create_date: '2018-06-30', due_date: '2019-06-30'
    },
    {
      id: '#3', name: 'Mr. Chindanai Amonwatee', username: 'Chindanai',
      status: 'in progress', create_date: '2018-07-30', due_date: '2019-06-30'
    },
    { id: '#4', name: 'Mr. Wasin  Poklin', username: 'Wasin', status: 'terminate', create_date: '2018-06-30', due_date: '2019-06-30' },
    {
      id: '#5', name: 'Mr. Sirakon  Khumaidoung', username: 'Sirakon',
      status: 'terminate', create_date: '2018-06-30', due_date: '2019-06-30'
    },
    {
      id: '#6', name: 'Mr. Santipap  Thongmai', username: 'Santipap',
      status: 'in progress', create_date: '2018-06-30', due_date: '2019-06-30'
    },
    {
      id: '#7', name: 'Ms. Thananya Supriyasilp', username: 'Thananya',
      status: 'in progress', create_date: '2018-06-30', due_date: '2019-06-30'
    },
    {
      id: '#8', name: 'Ms. Chatsakul Sombutpiboon', username: 'Chatsakul',
      status: 'in progress', create_date: '2018-06-30', due_date: '2019-06-30'
    },
    {
      id: '#9', name: 'Mr. Thapanut Lurejunda', username: 'Thapanut',
      status: 'in progress', create_date: '2018-06-30', due_date: '2019-06-30'
    },
    {
      id: '#10', name: 'Ms. Shanakan  Phetrattanarangsi',
      username: 'Shanakan', status: 'in progress', create_date: '2018-06-30', due_date: '2019-06-30'
    }
  ];
  open(content) {
    this.check_error = false;
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  edit(contente) {
    this.check_error = false;
    this.modalService.open(contente, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  delete(contentdelete) {
    this.modalService.open(contentdelete, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    });
  }

  createUser(c, username, Password, ConfirmPassword, name, eamil) {
    console.log(Password);
    if (Password === ConfirmPassword && Password !== undefined && ConfirmPassword !== undefined) {
      console.log('ssss');
      this.clearModal(c);
      this.check_error = false;
    } else {
      this.check_error = true;
      this.result_create = 'error password';
    }


  }


  editUser(c) {
    console.log('eeeeee');
    this.clearModal(c);
  }

  deleteUser(c) {
    console.log('dddddd');
    this.clearModal(c);
  }

  clearModal(content) {
    content('Close click');
    this.check_error = false;
  }


  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  ngOnInit() {
  }

}
