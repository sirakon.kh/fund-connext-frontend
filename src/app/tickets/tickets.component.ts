import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import {Http, Response, RequestOptions} from '@angular/http';
import { environment } from '../../environments/environment';

interface Country {
  name: string;
  flag: string;
  area: string;
  status: string;
  name_emp: string;
  population: number;
}

// interface Ticket {
//   ticket_subject: string;
//   ticket_type: string;
//   comapny: string;
//   owner: string;
//   level: string;
//   priority: number;
//   date: string;
//   ticket_status: string;
// }

const COUNTRIES: Country[] = [
  {
    name: 'Test11',
    flag: 'f/f3/Flag_of_Russia.svg',
    area: '12-10-2019',
    status: 'close',
    name_emp: 'Mr. Thatsanai Prabphai',
    population: 146989754
  },
  {
    name: 'Test22',
    flag: 'c/cf/Flag_of_Canada.svg',
    area: '13-06-2019',
    status: 'open',
    name_emp: 'Mr. Thanaphon Atharattanasak',
    population: 36624199
  },
  {
    name: 'Test33',
    flag: 'a/a4/Flag_of_the_United_States.svg',
    area: '10-05-2019',
    status: 'close',
    name_emp: 'Mr. Wasin  Poklin',
    population: 324459463
  },
  {
    name: ' Test44',
    flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    area: '19-02-2019',
    status: 'close',
    name_emp: 'Mr. Wasin  Poklin',
    population: 1409517397
  },
  {
    name: 'Test55',
    flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    area: '19-02-2019',
    status: 'resolve',
    name_emp: 'Mr. Thanaphon Atharattanasak',
    population: 1409517397
  },
  {
    name: ' Test66',
    flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    area: '19-02-2019',
    status: 'resolve',
    name_emp: 'Mrs. Sirakon  Khumaidoung',
    population: 1409517397
  },
  {
    name: ' Test77',
    flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
    area: '19-02-2019',
    status: 'close',
    name_emp: 'Mrs. Thananya Supriyasilp',
    population: 1409517397
  },
  {
    name: 'Test88',
    flag: 'c/cf/Flag_of_Canada.svg',
    area: '13-06-2019',
    status: 'close',
    name_emp: 'Mr. Thapanut Lurejunda',
    population: 36624199
  },
  {
    name: 'Test99',
    flag: 'c/cf/Flag_of_Canada.svg',
    area: '13-06-2019',
    status: 'close',
    name_emp: 'Ms. Chatsakul Sombutpiboon',
    population: 36624199
  }
];


@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.sass']
})
export class TicketsComponent implements OnInit {
  api_url = '';
  tickets = [
    {
      ticket_subject: 'test1',
      ticket_type: 'request',
      company: 'INET',
      owner: 'testoutsource',
      level: 'High',
      priority: 'Critical',
      date: '2019-01-09',
      ticket_status: 'Resolve'
    }
  ];
  outsourcetotal = {
    all: 50,
    inprocess: 10,
    open: 10,
    await_customer: 10,
    resolve: 20
  };

  page = 1;
  pageSize = 10;
  collectionSize = COUNTRIES.length;
  openticket = {
    ticket_subject: '',
    ticket_type: '',
    level: '',
    priority: '',
    status: '',
    company: '',
    owner: ''
  };
  message_insert = '';
  check_error = false;
  constructor(private modalService: NgbModal, private http: HttpClient) {
  }
  closeResult: string;

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
        return `with: ${reason}`;
    }
  }

  applyFilter(val) {
    console.log(val);
  }


  get countries(): Country[] {
    return COUNTRIES
      .map((country, i) => ({ id: i + 1, ...country }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  openticket_clear(content) {
    this.check_error = false;
    this.openticket.ticket_subject = '';
    this.openticket.ticket_type = '';
    this.openticket.level = '';
    this.openticket.priority = '';
    this.openticket.owner = '';
    this.openticket.status = '';
    this.openticket.company = '';
    this.message_insert = '';
    content.hide();
  }

  insertTicket(content) {
    console.log(this.openticket);
    this.api_url = 'http://203.154.34.91/api/v1';
    this.message_insert = '';
    this.openticket.owner = localStorage.getItem('worker_id');
    this.openticket.company = localStorage.getItem('company_id');
    if (this.openticket.ticket_subject !== '' && this.openticket.ticket_type !== '' && this.openticket.level !== '' &&
      this.openticket.priority !== '' && this.openticket.status !== '') {
      const token = localStorage.getItem('access_token');
      this.http.post(this.api_url + '/tickets', this.openticket).subscribe(data => {
        if (data['message'] === 'Open ticket success.') {
          alert('Complete !!!');
          this.openticket_clear(content);
        }
      });
    } else {
      this.check_error = true;
      if (this.openticket.ticket_subject === '') {
        this.message_insert = 'please enter subject';
      } else if (this.openticket.ticket_type === '') {
        this.message_insert = 'please enter type ticket';
      } else if (this.openticket.level === '') {
        this.message_insert = 'please enter ticket level case';
      } else if (this.openticket.priority === '') {
        this.message_insert = 'please enter ticket priority case';
      } else if (this.openticket.level === '') {
        this.message_insert = 'please enter ticket status ticket';
      }

    }
  }

  get_ticket() {
    this.api_url = '';
    this.http.get(this.api_url + '/tickets').subscribe(data => {
      console.log(data);
      this.tickets = data['result'];
    });
  }
  ngOnInit() {
  }

}
