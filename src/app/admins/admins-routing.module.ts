import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfomationComponent } from './infomation/infomation.component';


const routes: Routes = [
  {
    path: 'information',
    component: InfomationComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminsRoutingModule { }
