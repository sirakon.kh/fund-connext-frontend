import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Color } from 'ng2-charts/ng2-charts';
import { HttpClient } from '@angular/common/http';
import { RouterLink, Router } from '@angular/router';


@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.sass']
})
export class AdminsComponent implements OnInit {
  heading = 'Dashboard';
  // subheading = 'This is an example dashboard created using build-in elements and components.';
  icon = 'pe-7s-home icon-gradient bg-tempting-azure';
  // public datasets = [
  //   {
  //     label: 'My First dataset',
  //     data: [65, 59, 80, 81, 46, 55, 38, 59, 80],
  //     datalabels: {
  //       display: false,
  //     },
  //
  //   }
  // ];
  constructor(private http: HttpClient, private router: Router) { }
  outsourcetotal = {
    all: 50,
    onsite: 40,
    free: 10
  };
  datasettrade;


  public lineChartColors: Color[] = [
    { // dark grey
      backgroundColor: 'rgba(247, 185, 36, 0.2)',
      borderColor: '#f7b924',
      borderCapStyle: 'round',
      borderDash: [],
      borderWidth: 4,
      borderDashOffset: 0.0,
      borderJoinStyle: 'round',
      pointBorderColor: '#f7b924',
      pointBackgroundColor: '#fff',
      pointHoverBorderWidth: 4,
      pointRadius: 6,
      pointBorderWidth: 5,
      pointHoverRadius: 8,
      pointHitRadius: 10,
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#f7b924',
    },
  ];
  public datasets = [
    {
      label: 'My First dataset',
      data: [65, 59, 80, 81, 46, 55, 38, 59, 80],
      datalabels: {
        display: false,
      },

    }
  ];
  public labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August'];
  public options = {
    layout: {
      padding: {
        left: 0,
        right: 8,
        top: 0,
        bottom: 0
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          display: false,
          beginAtZero: true
        },
        gridLines: {
          display: false
        }
      }],
      xAxes: [{
        ticks: {
          display: false
        },
        gridLines: {
          display: false
        }
      }]
    },
    legend: {
      display: false
    },
    responsive: true,
    maintainAspectRatio: false
  };

  sendId(applicationId) {
    console.log(applicationId);
    this.router.navigate(['overview/dashboard/information']);
  }

  getUser() {
    this.http.get('http://localhost:8000/api/v1/applications').subscribe(data => {
      this.datasettrade = data;
      console.log(data);
    });
  }

  ngOnInit() {
    this.getUser();
  }

}
